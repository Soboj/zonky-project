import React from "react";
import "./css/App.css";

import FormControl from "@material-ui/core/FormControl";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";

import agent from "./agent";

import { Component } from "react";

class App extends Component {
  state = {
    data: [],
    castka: []
  };

  componentDidMount() {
    agent.Loan.getLoans().then(response => {
      this.setState({
        data: response
      });
    });
  }

  selectLoan = e => {
    this.setState({
      selectedRating: this.props.selectedRating
        ? this.props.selectedRating + e.target.value
        : e.target.value
    });
    this.lookForSameRate(e.target.value);
  };

  lookForSameRate = value => {
    const { data } = this.state;
    //Filtrace dat
    const amount = data.filter(n => n.rating === value);

    const allAmountArray = amount.map(loan => loan.amount);

    const reduce = (previousValue, currentValue) => previousValue + currentValue;

    const getObjectLength = allAmountArray.length;
    const isTrue = getObjectLength > 0;
    const allAmountTogether = isTrue ? allAmountArray.reduce(reduce) : 0;

    let divideAllAmount = isTrue
      ? allAmountTogether / Number(getObjectLength)
      : 0;

    let userAmount = Math.round(divideAllAmount, 0);

    if(userAmount === 0){
      this.setState({
        userAmount: "Zatím se zde nic nenachází ☹"
      });
    } else {
      this.setState({
        userAmount: userAmount + "Kč"
      });
    }
  };

  render() {

    return (
      <div className="App">
        <header className="App-header">
          <h1>Zonky.cz</h1>
        </header>
        <div className="left">
          <FormControl component="fieldset">
            <FormLabel
              component="legend"
              style={{ marginBottom: 20, fontSize: 30 }}
            >
              Výběr úroku
            </FormLabel>
            <RadioGroup
              aria-label="position"
              name="position"
              value={this.state.selectedRating || ""}
              onChange={this.selectLoan}
              style={{ marginLeft: 20 }}
            >
              <FormControlLabel
                value="AAAAAA"
                control={<Radio color="primary" />}
                label="2,99%"
                labelPlacement="end"
              />
              <FormControlLabel
                value="AAAAA"
                control={<Radio color="primary" />}
                label="3,99%"
                labelPlacement="end"
              />
              <FormControlLabel
                value="AAAA"
                control={<Radio color="primary" />}
                label="4,99%"
                labelPlacement="end"
              />
              <FormControlLabel
                value="AAA"
                control={<Radio color="primary" />}
                label="5,99%"
                labelPlacement="end"
              />
              <FormControlLabel
                value="AAE"
                control={<Radio color="primary" />}
                label="6,99%"
                labelPlacement="end"
              />
              <FormControlLabel
                value="AA"
                control={<Radio color="primary" />}
                label="8,49%"
                labelPlacement="end"
              />
              <FormControlLabel
                value="AE"
                control={<Radio color="primary" />}
                label="9,49%"
                labelPlacement="end"
              />
              <FormControlLabel
                value="A"
                control={<Radio color="primary" />}
                label="10,99%"
                labelPlacement="end"
              />
              <FormControlLabel
                value="B"
                control={<Radio color="primary" />}
                label="13,49%"
                labelPlacement="end"
              />
              <FormControlLabel
                value="C"
                control={<Radio color="primary" />}
                label="15,49%"
                labelPlacement="end"
              />
              <FormControlLabel
                value="D"
                control={<Radio color="primary" />}
                label="19,99%"
                labelPlacement="end"
              />
            </RadioGroup>
          </FormControl>
        </div>
        <div className="result">
          <p
            className="totalAmount"
          >
            {this.state.selectedRating
              ? "Pro rating *" +
                this.state.selectedRating +
                "*, máme půjčky v průměru: "
              : "Zatím jste si nevybral úrok"}
              <p className="totalAmount-child">{this.state.selectedRating ? this.state.userAmount : null}</p>
          </p>
        </div>
      </div>
    );
  }
}

export default App;
